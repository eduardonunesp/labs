#ifndef __COMMANDTREE_HPP__
#define __COMMANDTREE_HPP__

#include <string>
#include <vector>

struct CommandTree 
{
    typedef bool (commandFunc)(int, char**);
    typedef std::vector<std::string> Tokens;
    
    struct Command;
    typedef std::vector<Command*> CommandVector;

    struct Command
    {
        Command(const std::string& cmdStr, commandFunc*cmd = NULL, Command* parentCmd = NULL):
            commandString(cmdStr), command(cmd), parentCommand(parentCmd) {}

        std::string   commandString;
        commandFunc*  command;
        Command*      parentCommand;
        CommandVector childrenCommands;
    };

    CommandTree();
    ~CommandTree();

    void addCommand(const std::string& cmdStr, commandFunc* cmd);
    void removeCommand(const std::string& cmdStr);
    unsigned int countCommands(Command* cmd = NULL);

protected:
    Command* findParent(Command* parentcmd);
    Command* root;
};

#endif
