#include <iostream>
#include "commandtree.hpp"

static bool printSomething(int argc, char** argv)
{
    std::cout << "Printing something" << std::endl;
    return true;
}

int main(int argc, char* argv[]) {
    CommandTree* cmdTree = new CommandTree();
    cmdTree->addCommand("show", printSomething);
    std::cout << cmdTree->countCommands() << std::endl;

    delete cmdTree;
    return 0;
}
