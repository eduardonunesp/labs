#include "commandtree.hpp"
#include <iostream>
#include <sstream>
#include <algorithm>
#include <iterator>

CommandTree::CommandTree():
    root(NULL)
{
    root = new Command("tree", NULL, NULL);
}

CommandTree::~CommandTree()
{
}

CommandTree::Command* CommandTree::findParent(CommandTree::Command* childCmd) 
{
}

unsigned int CommandTree::countCommands(CommandTree::Command* cmd)
{
    unsigned int count = 0;
    if(!cmd) {
        cmd = root;
        count++;
        CommandVector::iterator itr;
        for(itr = cmd->childrenCommands.begin(); itr != cmd->childrenCommands.end(); itr++) {
            count = CommandTree::countCommands(*itr);
        }
    }

    return count;
}

void CommandTree::addCommand(const std::string& cmdStr, commandFunc* cmd) 
{
    std::istringstream iss(cmdStr);
    Tokens ts;
    std::copy(std::istream_iterator<std::string>(iss),
            std::istream_iterator<std::string>(),
            std::back_inserter<std::vector<std::string> >(ts));    

    Command* prevCmd = NULL;
    for(Tokens::iterator itr = ts.begin(); itr != ts.end(); itr++) {
        Command* newCmd = new Command(*itr, cmd, (!prevCmd ? root : prevCmd));
        prevCmd = newCmd;
    }
}

void CommandTree::removeCommand(const std::string& cmdStr)
{
}
