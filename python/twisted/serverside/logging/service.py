#!/bin/usr/env python
from twisted.internet import reactor
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint

class LoggingProtocol(LineReceiver):
    def lineReceived(self, line):
        self.factory.fp.write(line+'\n')
        print line

class LoggingFactory(Factory):
    protocol = LoggingProtocol

    def __init__(self, fileName):
        self.file = fileName

    def startFactory(self):
        self.fp = open(self.file, 'a')

    def stopFactory(self):
        self.fp.close()

if __name__ == "__main__":
    try:    
        # 8007 is the port you want to run under. Choose something >1024
        endpoint = TCP4ServerEndpoint(reactor, 8007)
        endpoint.listen(LoggingFactory("log.txt"))
        reactor.run()
    except KeyboardInterrupt:
        pass
