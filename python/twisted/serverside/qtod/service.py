#!/bin/usr/env python
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint

class QOTD(Protocol):
    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        self.factory.count = self.factory.count + 1
        self.transport.write("An apple a day keeps the doctor away\r\n") 
        self.transport.write("Connection number: %d\r\n" % self.factory.count) 
        self.transport.loseConnection()

    def connectionLost(self, reason):
        pass

    def abortConnection(self, reason):
        pass

    def dataReceived(self, data):
        pass



class QOTDFactory(Factory):
    count = 0

    def buildProtocol(self, addr):
        return QOTD(self)

if __name__ == "__main__":
    try:    
        # 8007 is the port you want to run under. Choose something >1024
        endpoint = TCP4ServerEndpoint(reactor, 8007)
        endpoint.listen(QOTDFactory())
        reactor.run()
    except KeyboardInterrupt:
        pass
