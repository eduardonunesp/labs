#!/bin/usr/env python
from twisted.internet import reactor
from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Protocol
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint

class Answser(LineReceiver):
    answers = {
        'How are you?': 'Fine', 
        None : "I don't know what you mean",
    }

    def lineReceived(self, line):
        if self.answers.has_key(line):
            self.sendLine(self.answers[line])
        else:
            self.sendLine(self.answers[None])    

class LineReceiverFactory(Factory):
    def buildProtocol(self, addr):
        return Answser()

if __name__ == "__main__":
    try:    
        # 8007 is the port you want to run under. Choose something >1024
        endpoint = TCP4ServerEndpoint(reactor, 8007)
        endpoint.listen(LineReceiverFactory())
        reactor.run()
    except KeyboardInterrupt:
        pass
