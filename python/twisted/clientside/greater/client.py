from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.endpoints import TCP4ClientEndpoint

class Greeter(Protocol):
    def sendMessage(self, msg):
        self.transport.write(msg)

class GreeterFactory(Factory):
    def buildProtocol(self, addr):
        return Greeter()

def gotProtocol(p):
    p.sendMessage("Hello\r\n")
    reactor.callLater(1, p.sendMessage, "This is sent in a second 1\r\n")
    reactor.callLater(2, p.sendMessage, "This is sent in a second 2\r\n")
    reactor.callLater(5, p.transport.loseConnection)

point = TCP4ClientEndpoint(reactor, "127.0.0.1", 8007)
d = point.connect(GreeterFactory())
#reactor.callback(30, point.cancel)
d.addCallback(gotProtocol)
reactor.run()    
