import sys
from PySide import QtCore, QtGui
from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.endpoints import TCP4ClientEndpoint

class Greeter(Protocol):
    def sendMessage(self, msg):
        self.transport.write(msg)

class GreeterFactory(Factory):
    def buildProtocol(self, addr):
        return Greeter()

class Dialog(QtGui.QDialog):
    serverIP = "localhost"
    serverPort = "8007"

    def __init__(self):
        super(Dialog, self).__init__()
        
        self.setWindowTitle("Chat")

        self.messageReader = QtGui.QTextEdit()
        self.messageEditor = QtGui.QTextEdit()
        self.sendButton    = QtGui.QPushButton("SEND")
        self.sendButton.clicked.connect(self.sendMessage)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(self.messageReader)
        mainLayout.addWidget(self.messageEditor)
        mainLayout.addWidget(self.sendButton)
        self.setLayout(mainLayout)

        self.point = TCP4ClientEndpoint(reactor, self.serverIP, self.serverPort)
        self.endpoint = self.point.connect(GreeterFactory())
        
        reactor.run()    

    def setName(self):
        text, ok = QtGui.QInputDialog.getText(self, "",
            "User name:", QtGui.QLineEdit.Normal,
        QtCore.QDir.home().dirName())
        if ok and text != '': 
            print text

    def sendMessage(self):
        text = self.messageEditor.toPlainText()
        self.messageEditor.clear()

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    mainWin = Dialog()
    mainWin.show()
    sys.exit(app.exec_())
